﻿using AutoMapper.Configuration;
using Microsoft.EntityFrameworkCore;
using VuAPI.data;

namespace Inmergers.Data
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private readonly DbContext _dataContext;
        private readonly IConfiguration _configuration;

        public DatabaseFactory()
        {
            _dataContext = new vuAlbumcontext.DbContext();
        }

        public DbContext GetDbContext()
        {
            return _dataContext;
        }
    }
}