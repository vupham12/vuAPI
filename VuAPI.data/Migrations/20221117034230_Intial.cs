﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace vuAPI.Migrations
{
    public partial class Intial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "vus",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Founded = table.Column<DateTime>(nullable: false),
                    MainGenre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_vus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Albums",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(maxLength: 400, nullable: true),
                    vuId = table.Column<Guid>(name: "vuId)", nullable: true),
                    vuId0 = table.Column<Guid>(name: "vuId", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Albums", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Albums_vus_vuId)",
                        column: x => x.vuId,
                        principalTable: "vus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Albums",
                columns: new[] { "Id", "Description", "Title", "vuId", "vuId)" },
                values: new object[,]
                {
                    { new Guid("459438d8-09a4-4593-8ba5-d8a9ccd4156b"), "lop du ve ri must", "Master vu", new Guid("6c903294-6561-42cf-a422-850d3b2ebbe5"), null },
                    { new Guid("fec3cc8c-80e6-42cc-aa48-3f1efb5963f8"), "lop du ve ri must more", "Master khang", new Guid("a5f1a513-652c-4621-979e-c162768912f5"), null },
                    { new Guid("55aadbcc-d903-48b8-82e5-5732e7da6e36"), "lop du ve ri must more than", "Master bunbo", new Guid("aac60034-ac7e-4f8e-9364-516a8bb550fe"), null }
                });

            migrationBuilder.InsertData(
                table: "vus",
                columns: new[] { "Id", "Founded", "MainGenre", "Name" },
                values: new object[,]
                {
                    { new Guid("6c903294-6561-42cf-a422-850d3b2ebbe5"), new DateTime(2001, 1, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hellovu", "DemonLeader" },
                    { new Guid("a5f1a513-652c-4621-979e-c162768912f5"), new DateTime(2002, 6, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hellokhang", "tuigg" },
                    { new Guid("aac60034-ac7e-4f8e-9364-516a8bb550fe"), new DateTime(2000, 4, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hellotan", "bunbo" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Albums_vuId)",
                table: "Albums",
                column: "vuId)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Albums");

            migrationBuilder.DropTable(
                name: "vus");
        }
    }
}
