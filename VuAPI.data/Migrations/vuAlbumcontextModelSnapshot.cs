﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using VuAPI.data;

namespace vuAPI.Migrations
{
    [DbContext(typeof(vuAlbumcontext))]
    partial class vuAlbumcontextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.4")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("vuAPI.Entities.Album", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(400)")
                        .HasMaxLength(400);

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("nvarchar(200)")
                        .HasMaxLength(200);

                    b.Property<Guid>("vuId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid?>("vuId)")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("vuId)");

                    b.ToTable("Albums");

                    b.HasData(
                        new
                        {
                            Id = new Guid("459438d8-09a4-4593-8ba5-d8a9ccd4156b"),
                            Description = "lop du ve ri must",
                            Title = "Master vu",
                            vuId = new Guid("6c903294-6561-42cf-a422-850d3b2ebbe5")
                        },
                        new
                        {
                            Id = new Guid("fec3cc8c-80e6-42cc-aa48-3f1efb5963f8"),
                            Description = "lop du ve ri must more",
                            Title = "Master khang",
                            vuId = new Guid("a5f1a513-652c-4621-979e-c162768912f5")
                        },
                        new
                        {
                            Id = new Guid("55aadbcc-d903-48b8-82e5-5732e7da6e36"),
                            Description = "lop du ve ri must more than",
                            Title = "Master bunbo",
                            vuId = new Guid("aac60034-ac7e-4f8e-9364-516a8bb550fe")
                        });
                });

            modelBuilder.Entity("vuAPI.Entities.vu", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("Founded")
                        .HasColumnType("datetime2");

                    b.Property<string>("MainGenre")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(100)")
                        .HasMaxLength(100);

                    b.HasKey("Id");

                    b.ToTable("vus");

                    b.HasData(
                        new
                        {
                            Id = new Guid("6c903294-6561-42cf-a422-850d3b2ebbe5"),
                            Founded = new DateTime(2001, 1, 18, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            MainGenre = "Hellovu",
                            Name = "DemonLeader"
                        },
                        new
                        {
                            Id = new Guid("a5f1a513-652c-4621-979e-c162768912f5"),
                            Founded = new DateTime(2002, 6, 10, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            MainGenre = "Hellokhang",
                            Name = "tuigg"
                        },
                        new
                        {
                            Id = new Guid("aac60034-ac7e-4f8e-9364-516a8bb550fe"),
                            Founded = new DateTime(2000, 4, 19, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            MainGenre = "Hellotan",
                            Name = "bunbo"
                        });
                });

            modelBuilder.Entity("vuAPI.Entities.Album", b =>
                {
                    b.HasOne("vuAPI.Entities.vu", "vu")
                        .WithMany("Albums")
                        .HasForeignKey("vuId)");
                });
#pragma warning restore 612, 618
        }
    }
}
