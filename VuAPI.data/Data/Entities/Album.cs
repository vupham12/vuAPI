﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vuAPI.Entities
{
    public class Album
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Title { get; set; }

        [MaxLength(400)]
        public string Description { get; set; }

        [ForeignKey("vuId)")]
        public vu vu { get; set; }
        public Guid vuId { get; set; }

    }
}
