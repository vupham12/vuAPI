﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using vuAPI.Entities;

namespace VuAPI.data
{
    // tạo database
    public class vuAlbumcontext : DbContext

    {
        public vuAlbumcontext(DbContextOptions<vuAlbumcontext> options) :
            base(options)
        {
        }

        public DbSet<vu> vus { get; set; }
        public DbSet<Album> Albums { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<vu>().HasData(new vu()
            {
                Id = Guid.Parse("6c903294-6561-42cf-a422-850d3b2ebbe5"),
                Name = "DemonLeader",
                Founded = new DateTime(2001, 1, 18),
                MainGenre = "Hellovu"
            },
            new vu
            {
                Id = Guid.Parse("a5f1a513-652c-4621-979e-c162768912f5"),
                Name = "tuigg",
                Founded = new DateTime(2002, 6, 10),
                MainGenre = "Hellokhang"

            },
            new vu
            {
                Id = Guid.Parse("aac60034-ac7e-4f8e-9364-516a8bb550fe"),
                Name = "bunbo",
                Founded = new DateTime(2000, 4, 19),
                MainGenre = "Hellotan"

            });

            modelBuilder.Entity<Album>().HasData(new Album()
            {
                Id = Guid.Parse("459438d8-09a4-4593-8ba5-d8a9ccd4156b"),
                Title = "Master vu",
                Description = "lop du ve ri must",
                vuId = Guid.Parse("6c903294-6561-42cf-a422-850d3b2ebbe5")
            },
                new Album
                {
                    Id = Guid.Parse("fec3cc8c-80e6-42cc-aa48-3f1efb5963f8"),
                    Title = "Master khang",
                    Description = "lop du ve ri must more",
                    vuId = Guid.Parse("a5f1a513-652c-4621-979e-c162768912f5")
                },
                new Album
                {
                    Id = Guid.Parse("55aadbcc-d903-48b8-82e5-5732e7da6e36"),
                    Title = "Master bunbo",
                    Description = "lop du ve ri must more than",
                    vuId = Guid.Parse("aac60034-ac7e-4f8e-9364-516a8bb550fe")
                });

            base.OnModelCreating(modelBuilder);
        }

        internal class DbContext : Microsoft.EntityFrameworkCore.DbContext
        {
        }
    }
}
