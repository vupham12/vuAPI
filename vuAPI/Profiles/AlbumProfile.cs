﻿using AutoMapper;
using vuAPI.Models;

namespace vuAPI.Profiles
{
    public class AlbumProfile :Profile
    {
        public AlbumProfile()
        {
            CreateMap<Entities.Album,Models.AlbumsDto>().ReverseMap();
            CreateMap<AlbumForCreatingDto, Entities.Album>();
            CreateMap<Models.AlbumForUpdatingDto, Entities.Album>().ReverseMap();
        }
    }
}
