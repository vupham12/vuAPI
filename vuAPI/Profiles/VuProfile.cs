﻿using AutoMapper;
using vuAPI.Helpers;

namespace vuAPI.Profires
{
    public class VuProfile: Profile
    {
        public VuProfile()
        {
            CreateMap<Entities.vu, Models.VuDto>()
                .ForMember(
                    dest => dest.FoundedYearago,
                    opt => opt.MapFrom(src => $"{src.Founded.ToString("yyyy")}({src.Founded.GetYearAgo()}) years ago"));
            CreateMap<Models.VuForCreatingDto, Entities.vu>();
        }
    }
}
