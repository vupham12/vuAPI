﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using vuAPI.Entities;
using vuAPI.Helpers;
using vuAPI.Models;
using vuAPI.Servicers;

namespace vuAPI.Controllers
{
    [ApiController]
    [Route("api/vucollections")]
    public class vuCollectionsController :ControllerBase
    {
        private readonly IvuAlbumReponsitory _vuAlbumReponsitory;
        private readonly IMapper _mapper;

        public vuCollectionsController(IvuAlbumReponsitory vuAlbumReponsitory, IMapper mapper)
        {
            _vuAlbumReponsitory = vuAlbumReponsitory ??
                throw new ArgumentNullException(nameof(vuAlbumReponsitory));
            _mapper = mapper ??
                throw new ArgumentNullException(nameof(mapper));
        }
        [HttpGet("({ids})", Name = "GetVuCollection")]
        public IActionResult GetVuCollection([FromRoute]
             /* [ModelBinder(BinderType = typeof(ArrayModelBinder))]*/ IEnumerable<Guid> ids)
        {
            if (ids == null)
                return BadRequest();

            var vuEntities = _vuAlbumReponsitory.GetVus(ids);

            if (ids.Count() != vuEntities.Count())
                return NotFound();

            var vuToReturn = _mapper.Map<IEnumerable<VuDto>>(vuEntities);

            return Ok(vuToReturn);
        }

        [HttpPost]
        public ActionResult<IEnumerable<VuDto>> CreateVuCollection([FromBody] 
        IEnumerable<VuForCreatingDto> vuCollection)
        {
            var vuEntities = _mapper.Map<IEnumerable<Entities.vu>>(vuCollection);

            foreach(var vu in vuEntities) 
            {
                _vuAlbumReponsitory.Addvu(vu);
            }
            _vuAlbumReponsitory.Save();
            var vuCollectionToReturn = _mapper.Map<IEnumerable<VuDto>>(vuEntities);
            var IdsString = string.Join(",", vuCollectionToReturn.Select(a => a.Id));



            return CreatedAtRoute("GetVuCollection",new {ids = IdsString},
                vuCollectionToReturn);

        }
    }
}
