﻿using AutoMapper;
using Inmergers.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using vuAPI.Entities;
using vuAPI.Helpers;
using vuAPI.Models;
using vuAPI.Servicers;
using VuAPI.Business;

namespace vuAPI.Controllers
{
    [ApiController]
    [Route("api/vus")]
    public class vuControllers : ControllerBase
    {
        private readonly IvuAlbumReponsitory _vuAlbumReponsitory;
        private readonly IMapper _mapper;
       /* private readonly IPropertyMappingService _propertyMappingService;
        private readonly IPropertyValidationService _propertyValidationService;*/

        public vuControllers(IvuAlbumReponsitory vuAlbumReponsitory, IMapper mapper/*,*/
            //IPropertyMappingService propertyMappingService,
           /* IPropertyValidationService propertyValidationService*/)
        {
            _vuAlbumReponsitory = vuAlbumReponsitory ??
                throw new ArgumentNullException(nameof(vuAlbumReponsitory));
            _mapper = mapper ??
                throw new ArgumentNullException(nameof(mapper));
            //_propertyMappingService = propertyMappingService ??
            //throw new ArgumentNullException(nameof(propertyMappingService));
            //_propertyValidationService = propertyValidationService ??
            //    throw new ArgumentNullException(nameof(propertyValidationService));

        }

        [HttpGet(Name ="Getvus")]
        [HttpHead]
        public async Task < IActionResult >Getvus( [FromQuery] PostQueryModel vusResourceParameters) 
        {
         
            var vusFromRepo = await _vuAlbumReponsitory.Get(vusResourceParameters);

            return Helper.TransformData(vusFromRepo);
        }

        [HttpGet("{vuId}", Name ="Getvu")]
        public async Task< IActionResult> GetId(Guid vuId) 
        {
       
            var result = await _vuAlbumReponsitory.GetById(vuId);
            // Hander response
            return Helper.TransformData(result);
        }

        

        [HttpPost]
        public async Task<IActionResult> CreateVu([FromBody] PostCreateModel vu)
        {
             
            var result = await _vuAlbumReponsitory.Create(vu);
            // Hander response
            return Helper.TransformData(result);
        }

        [HttpPut]
        public async Task <IActionResult> UpdateVu( Guid id, [FromBody] PostUpdateModel postUpdate)
        {
            var result = await _vuAlbumReponsitory.Update(id, postUpdate);
            // Hander response
            return Helper.TransformData(result);
        }

        [HttpDelete("{vuId}")]
        public async Task<IActionResult> Deletevu(Guid id)
        {
            var result = await _vuAlbumReponsitory.DeleteData(id);
            // Hander response
            return Helper.TransformData(result);
        }


        [HttpOptions]
        public IActionResult GetVusOptions()
        {
            Response.Headers.Add("Allow", "GET,POST,DELETE,HEAD,OPTIONS");
                return Ok();
        }

        // chức năng phân trang
        private string CreateVusUri(VusResourceParameters vusResourceParameters, UriType uriType)
        {
            switch (uriType) 
            {
                case UriType.PreviousPage:
                    return Url.Link("GetVus", new
                    {
                        fields = vusResourceParameters.Fields,
                        orderBy = vusResourceParameters.OrderBy,
                        pageNumber = vusResourceParameters.PageNumber - 1,
                        pageSize = vusResourceParameters.PageSize,
                        mainGenre = vusResourceParameters.MainGenre,
                        searchQuery = vusResourceParameters.SearchQuery
                    });
                case UriType.NextPage:
                    return Url.Link("GetVus", new
                    {
                        fields = vusResourceParameters.Fields,
                        orderBy = vusResourceParameters.OrderBy,
                        pageNumber = vusResourceParameters.PageNumber + 1,
                        pageSize = vusResourceParameters.PageSize,
                        mainGenre = vusResourceParameters.MainGenre,
                        searchQuery = vusResourceParameters.SearchQuery
                    });
                default:
                    return Url.Link("GetVus", new
                    {
                        fields = vusResourceParameters.Fields,
                        orderBy = vusResourceParameters.OrderBy,
                        pageNumber = vusResourceParameters.PageNumber,
                        pageSize = vusResourceParameters.PageSize,
                        mainGenre = vusResourceParameters.MainGenre,
                        searchQuery = vusResourceParameters.SearchQuery
                    });
            }
        }
    }
}
