﻿using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

using vuAPI.Models;
using vuAPI.Servicers;

namespace vuAPI.Controllers
{
    [ApiController]
    [Route("api/vus/{vuId}/albums")]
    [ResponseCache(CacheProfileName = "90SecondsCacheProfile")]
    public class AlbumController : ControllerBase
    {
        private readonly IvuAlbumReponsitory _vuAlbumReponsitory;
        private readonly IMapper _mapper;

        public AlbumController(IvuAlbumReponsitory vuAlbumReponsitory, IMapper mapper)
        {
            _vuAlbumReponsitory = vuAlbumReponsitory ??
                throw new ArgumentNullException(nameof(vuAlbumReponsitory));
            _mapper = mapper ??
                throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        public ActionResult<IEnumerable<AlbumsDto>> GetAlbumsForVu(Guid vuId)
        {
            if (_vuAlbumReponsitory.vuExists(vuId))
                return NotFound();

            var albumsFromRepo = _vuAlbumReponsitory.GetAlbums(vuId);
            return Ok(_mapper.Map<IEnumerable<AlbumsDto>>(albumsFromRepo));
        }
        [HttpGet("{albumId}", Name ="GetAlbumForVu")]
        [ResponseCache(Duration =120)]
        public ActionResult<AlbumsDto> GetAlbumForVu(Guid vuId, Guid albumId) 
        {
            if (_vuAlbumReponsitory.vuExists(vuId))
                return NotFound();

            var albumFromRepo = _vuAlbumReponsitory.GetAlbum(vuId, albumId);
            if (albumFromRepo == null) 
                return NotFound();

            return Ok(_mapper.Map<AlbumsDto>(albumFromRepo));
        }

        [HttpPost]
        public ActionResult<AlbumsDto> CreateAlbumForVu(Guid vuId, 
            [FromBody] AlbumForCreatingDto album)
        {
            if (!_vuAlbumReponsitory.vuExists(vuId))
                return NotFound();

            var albumEntity = _mapper.Map<Entities.Album>(album);
            _vuAlbumReponsitory.AddAlbum(vuId, albumEntity);
            _vuAlbumReponsitory.Save();

            var albumToReturn = _mapper.Map<AlbumsDto>(albumEntity);
            return CreatedAtRoute("GetAlbumForVu",new {vuId = vuId, albumId = albumToReturn.Id}
            ,albumToReturn);
        }

        [HttpPut("{albumId}")]
        public ActionResult UpdateAlbumForVu(Guid vuId, Guid albumId,
            [FromBody] AlbumForUpdatingDto album)
        {
            if (_vuAlbumReponsitory.vuExists(vuId))
                return NotFound();

            var albumFromRepo = _vuAlbumReponsitory.GetAlbum(vuId, albumId);
            if (albumFromRepo == null)
            {
                var albumToAdd = _mapper.Map<Entities.Album>(album);
                albumToAdd.Id = albumId;
                _vuAlbumReponsitory.AddAlbum(vuId, albumToAdd);
                _vuAlbumReponsitory.Save();

                var albumToReturn = _mapper.Map<AlbumsDto>(albumToAdd);

                return CreatedAtRoute("GetAlbumForVu", new { vuId=vuId, albumId = 
                    albumToReturn.Id }, albumToReturn);
            }

            _mapper.Map(album, albumFromRepo);
            _vuAlbumReponsitory.UpdateAlbum(albumFromRepo);
            _vuAlbumReponsitory.Save();

            return NoContent();
        }

        [HttpPatch("{albumId}")]
        public ActionResult PartiallyUpdateAlbumForBand(Guid vuId, Guid albumId,
            [FromBody] JsonPatchDocument<AlbumForUpdatingDto> patchDocument)
        {
            if (_vuAlbumReponsitory.vuExists(vuId))
                return NotFound();

            var albumFromRepo = _vuAlbumReponsitory.GetAlbum(vuId, albumId);
            if (albumFromRepo == null)
            {
                var albumDto = new AlbumForUpdatingDto();
                patchDocument.ApplyTo(albumDto);
                var albumToAdd = _mapper.Map<Entities.Album>(albumDto);
                albumToAdd.Id = albumId;
                
                _vuAlbumReponsitory.AddAlbum(vuId,albumToAdd);
                _vuAlbumReponsitory.Save();

                var albumToReturn = _mapper.Map<AlbumsDto>(albumToAdd);

                return CreatedAtRoute("GetAlbumForVu", new { vuId = vuId,
                    albumId = albumToReturn.Id }, albumToReturn);
            }   

            var albumToPatch = _mapper.Map<AlbumForUpdatingDto>(albumFromRepo);
            patchDocument.ApplyTo(albumToPatch,ModelState);

            if (!TryValidateModel(albumToPatch))
                return ValidationProblem(ModelState);

            _mapper.Map(albumToPatch, albumFromRepo);
            _vuAlbumReponsitory.UpdateAlbum(albumFromRepo);
            _vuAlbumReponsitory.Save();

            return NoContent();
        }
        [HttpDelete("{albumId}")]
        public ActionResult DeleteAlbumForVu(Guid vuId, Guid albumId)
        {
            if (!_vuAlbumReponsitory.vuExists(vuId))
                return NotFound();

            var albumFromRepo = _vuAlbumReponsitory.GetAlbum(vuId, albumId);

            if (albumFromRepo == null)
                return NotFound();

            _vuAlbumReponsitory.DeleteAlbum(albumFromRepo);
            _vuAlbumReponsitory.Save();

            return NoContent();
        }
    }
}
