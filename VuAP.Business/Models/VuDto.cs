﻿using System;

namespace vuAPI.Models
{
    public class VuDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }    
        public string FoundedYearago { get; set; }
        public string MainGenre { get; set; }
    }
}
