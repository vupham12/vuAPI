﻿
using System;


namespace vuAPI.Models
{
    public class AlbumsDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid vuId { get; set; }
    }
}
