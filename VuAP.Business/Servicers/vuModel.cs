﻿using Inmergers.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace VuAPI.Business
{
    internal class vuModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string FoundedYearago { get; set; }
        public string MainGenre { get; set; }
    }

    public class PostCreateModel
    {
        public string Name { get; set; }
        public DateTime Founded { get; set; }
        public string MainGenre { get; set; }
       
    }

    public class PostUpdateModel
    {
        public string Name { get; set; }
        public DateTime Founded { get; set; }
        public string MainGenre { get; set; }
    }

   

    public class PostQueryModel : PaginationRequest
    {
        public Guid? CategoryId { get; set; }
    }

}

