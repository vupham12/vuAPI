﻿using Inmergers.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using vuAPI.Entities;
using vuAPI.Helpers;
using VuAPI.Business;

namespace vuAPI.Servicers
{
    public interface IvuAlbumReponsitory
    {
        IEnumerable<Album> GetAlbums(Guid vuId);
        Album GetAlbum(Guid vuId, Guid albumId);
        void AddAlbum(Guid vuId, Album album);
        void UpdateAlbum(Album album);
        void DeleteAlbum(Album album);

        IEnumerable<vu> GetVus();
        vu GetVu(Guid vuId);
        IEnumerable<vu> GetVus(IEnumerable<Guid> vuIds);
        PageList<vu> GetVus(VusResourceParameters vusResourceParameters);

        Task<Response> Get(PostQueryModel filter);
        Task<Response> GetById(Guid id);
        Task<Response> Create(PostCreateModel model);
        Task<Response> Update(Guid id, PostUpdateModel model);

        Task<Response> DeleteData(Guid id);

        void Addvu(vu vu);
        void Updatevu(vu vu);
        void Deletevu(vu vu);

        bool vuExists(Guid vuId);
        bool AlbumExists(Guid albumId);
        bool Save();
        
    }
}
