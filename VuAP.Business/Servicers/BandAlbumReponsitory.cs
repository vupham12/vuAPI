﻿using AutoMapper;
using Inmergers.Common;
using Org.BouncyCastle.Crypto;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using vuAPI.Entities;
using vuAPI.Helpers;
using Code = System.Net.HttpStatusCode;
using VuAPI.data;
using Abp.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using vuAPI.Models;
using Abp.Domain.Entities;
using LibGit2Sharp;
using Microsoft.AspNetCore.Mvc;
using VuAPI.Business;

namespace vuAPI.Servicers
{
    public class BandAlbumReponsitory : IvuAlbumReponsitory
    {
        private readonly vuAlbumcontext _context;
        private readonly IMapper _mapper;


        public BandAlbumReponsitory(vuAlbumcontext context, IMapper mapper) 
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper?? throw new ArgumentNullException(nameof(mapper)); 
            
           
        }
        public void AddAlbum(Guid vuId, Album album)
        {
            if (vuId == Guid.Empty)
                throw new ArgumentNullException(nameof(vuId));

            if (album == null)
                throw new ArgumentNullException(nameof(album));
            album.vuId = vuId;
            _context.Albums.Add(album);
        }

        public void Addvu(vu vu)
        {
            if (vu == null)
                throw new ArgumentNullException(nameof(vu));

            _context.vus.Add(vu);
        }

        public async Task<Response> Get(PostQueryModel filter)
        {
            try
            {
                var predicate = BuildQuery(filter);
                var result = _context.vus.Where(predicate).GetPage(filter);
                return new ResponsePagination<vu>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        private Expression<Func<vu, bool>> BuildQuery(PostQueryModel query)
        {
            var predicate = PredicateBuilder.New<vu>(true);

            if (query.CategoryId.HasValue && query.CategoryId.Value != Guid.Empty)
            {
                predicate = predicate.And(s => s.Id == query.Id);
            }

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(s => s.Name.Contains(query.FullTextSearch)
                || s.MainGenre.Contains(query.FullTextSearch.ToLower()));

            return predicate;
        }

        public async Task<Response> Create(PostCreateModel model)
        {
            try
            {

                var entityModel = new vu
                {
                    Founded = model.Founded,
                    Name = model.Name,
                    MainGenre = model.MainGenre
                };
                _context.Add(entityModel);

                var status = await _context.SaveChangesAsync();

                if (status > 0)
                {
                    var data = _mapper.Map<vu, VuDto>(entityModel);

                    return new ResponseObject<VuDto>(data, "Thêm thành công");
                    
                }

                return new ResponseError(Code.NotFound, "Thêm mới thất bại");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        public async Task<Response> Update(Guid id, PostUpdateModel model)
        {
            try
            {
                var entityModel = await _context.vus.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (entityModel == null)
                    return new ResponseError(Code.NotFound, "Không tìm thấy bản ghi");


                entityModel.Founded = model.Founded;
                entityModel.Name = model.Name;
                entityModel.MainGenre = model.MainGenre;

                var status = await _context.SaveChangesAsync();
                

                if (status > 0)
                {
                    var data = _mapper.Map<vu, VuDto>(entityModel);

                    return new ResponseObject<VuDto>(data, "cập nhật thành công");

                }

                return new ResponseError(Code.NotFound, "Cập nhật thất bại");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        public async Task<Response> GetById(Guid id)
        {
            try
            {
                var entity = await _context.vus.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (entity == null)
                    return new ResponseError(Code.NotFound, "Không tìm thấy bản ghi");

                var data = _mapper.Map<vu, VuDto>(entity);

                return new ResponseObject<VuDto>(data);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        
        public async Task<Response> DeleteData(Guid id)
        {
                var entity = await _context.vus.Where(x => x.Id == id).FirstOrDefaultAsync();
                if (entity == null)

                    return new Response(Code.OK, "Không tồn tại");
                _context.vus.Remove(entity);
                var status = await _context.SaveChangesAsync();

                return new Response(Code.OK, "xóa thành công");
        }


        public bool AlbumExists(Guid albumId)
        {
           if (albumId == Guid.Empty)
                throw new ArgumentNullException(nameof(albumId));
            return _context.Albums.Any(a => a.Id == albumId);
        }

        public bool vuExists(Guid vuId)
        {
            if (vuId == Guid.Empty)
                throw new ArgumentNullException(nameof(vuId));
            return _context.Albums.Any(b => b.Id == vuId);
        }

        public void DeleteAlbum(Album album)
        {
           if (album == null) 
                throw new ArgumentNullException(nameof(album));

           _context.Albums.Remove(album);
        }

        public void Deletevu(vu vu)
        {
            if (vu == null)
                throw new ArgumentNullException(nameof(vu));

            _context.vus.Remove(vu);
        }



        public Album GetAlbum(Guid vuId, Guid albumId)
        {
            if (vuId == Guid.Empty)
                throw new ArgumentNullException(nameof(vuId));

            if (albumId == null)
                throw new ArgumentNullException(nameof(albumId));
            return _context.Albums.Where(a => a.vuId == vuId && a.Id ==
            albumId).FirstOrDefault();
        }

        public IEnumerable<Album> GetAlbums(Guid vuId)
        {
            if (vuId == Guid.Empty)
                throw new ArgumentNullException(nameof(vuId));

            return _context.Albums.Where(a => a.vuId == vuId)
                                        .OrderBy(a => a.Title).ToList();
        }

        public vu GetVu(Guid vuId)
        {
            if (vuId == Guid.Empty)
                throw new ArgumentNullException(nameof(vuId));
            return _context.vus.FirstOrDefault(b => b.Id== vuId);
        }

        public IEnumerable<vu> GetVus()
        {
            return _context.vus.ToList();
        }

        public IEnumerable<vu> GetVus(IEnumerable<Guid> vuIds)
        {
            if (vuIds == null)
                throw new ArgumentException(nameof(vuIds));

            return _context.vus.Where(b =>vuIds.Contains(b.Id)).
                    OrderBy(b => b.Name).ToList();
        }

        public PageList<vu> GetVus(VusResourceParameters vusResourceParameters)
        {
            if (vusResourceParameters == null)
                throw new ArgumentException(nameof(vusResourceParameters));

            var collection = _context.vus as IQueryable<vu>;

            if (!string.IsNullOrWhiteSpace(vusResourceParameters.MainGenre))
            {
               var mainGenre = vusResourceParameters.MainGenre.Trim();
                collection = collection.Where(b => b.MainGenre == mainGenre);
            }
            if (!string.IsNullOrWhiteSpace(vusResourceParameters.SearchQuery))
            {
                var searchQuery = vusResourceParameters.SearchQuery.Trim();
                collection = collection.Where(b => b.Name.Contains(searchQuery));
            }

            if (!string.IsNullOrWhiteSpace(vusResourceParameters.OrderBy)) 
            {
                var searchQuery = vusResourceParameters.SearchQuery.Trim();
                collection = collection.Where(b => b.Name.Contains(searchQuery));
            }

            return PageList<vu>.Create(collection,vusResourceParameters.PageNumber,
                vusResourceParameters.PageSize);
        }

        public bool Save()
        {
           return (_context.SaveChanges() >= 0);
        }

        public void UpdateAlbum(Album album)
        {
            // not implemented
        }

        public void Updatevu(vu vu)
        {
            // not implemented
        }

    }
}
