﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace vuAPI.Helpers
{
    public class PageList<T>:List<T>
    {
        public int CurrentPage { get; set; }
        public int TotalPage { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public bool HasPrevios => (CurrentPage > 1);
        public bool HasNext => (CurrentPage < TotalCount);

        public PageList(List<T> item, int totalCount, int currentPage, int pageSize) 
        {
            TotalCount= totalCount;
            CurrentPage= currentPage;
            PageSize= pageSize;
            TotalPage = (int)Math.Ceiling(totalCount /(double)pageSize);
            AddRange(item);
        }

        public static PageList<T> Create(IQueryable<T> source, int pageNumber, int pageSize)
        {
            var count = source.Count();
            var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            return new PageList<T>(items, count, pageNumber, pageSize);
        }
    }
}
