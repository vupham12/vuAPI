﻿using System;

namespace vuAPI.Helpers
{
    public static class FoundYearAgo
    {
        public static int GetYearAgo(this DateTime dateTime)
        {
            var currentDate = DateTime.Now;
            int yearAgo = currentDate.Year - dateTime.Year;

            return yearAgo;
        }
    }
}
